﻿using Project.Base;
using Project.LifeCycle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
    public class Program
    {
        static void Main(string[] args)
        {
            Order order = new Order(
                new List<BaseOrderDetail>()
                {
                    new BaseOrderDetail(new BaseProduct(1,"Mobile",140000),4),
                    new BaseOrderDetail(new BaseProduct(5,"Tablet",1498000),6),
                });

            Console.WriteLine("Enter your Id");
            int SellerId = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter your Name");
            string SellerName = Console.ReadLine();
            Console.WriteLine("Enter your Family");
            string SelleFamily = Console.ReadLine();

            order.InitialSeller(SellerId, SellerName, SelleFamily);

            order.InitialBuyer(2, "Ali", "Osouli");

            order.Print();

            Console.ReadKey();

        }
    }
}
