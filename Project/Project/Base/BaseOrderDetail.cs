﻿using Project.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Base
{
    public class BaseOrderDetail : IOrderDetail <int>
    {
        #region [- ctor -]
        public BaseOrderDetail(BaseProduct baseProduct, int quantity)
        {
            BaseProduct = baseProduct;
            Quantity = quantity;

        }
        #endregion

        public BaseProduct BaseProduct { get; set; }

        public int Quantity { get; set; }

        #region [- Price -]
        public int Price { get { return BaseProduct.UnitPrice * Quantity; } } 
        #endregion

    }
}
