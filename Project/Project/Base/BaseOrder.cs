﻿using Project.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Base
{
    public class BaseOrder : IOrder<int, BasePerson, List<BaseOrderDetail>>
    {
        #region [- ctor -]
        public BaseOrder(List<BaseOrderDetail> baseOrderDetails)
        {
            BaseOrderDetails = baseOrderDetails;
            
        }
        #endregion

        #region [- props -]
        public BasePerson BaseSeller { get; set; }
        public BasePerson BaseBuyer { get; set; }
        public List<BaseOrderDetail> BaseOrderDetails { get; set; }
        public int Id { get; set; }

        #region [- TotalPrice -]
        public int TotalPrice
        {
            get
            {
                int totalPrice = 0;
                foreach (var items in BaseOrderDetails)
                {
                    totalPrice += items.Price;
                }
                return totalPrice;
            }
        }
        #endregion 
        #endregion

        #region [- InitialSeller -]
        public void InitialSeller(int id, string fName, string lName)
        {
            BaseSeller = new BasePerson(id, fName, lName);
        }
        #endregion

        #region [- InitialBuyer -]
        public void InitialBuyer(int id, string fName, string lName)
        {
            BaseSeller = new BasePerson(id, fName, lName);
        }
        #endregion

        #region [- Print() -]
        public BasePrint Print()
        {
            return new BasePrint(TotalPrice, InitialSeller, InitialBuyer);
        }
        #endregion


    }
}
