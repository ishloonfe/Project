﻿using Project.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Base
{
    public class BasePerson : IPerson <int, string>
    {
        #region [- ctors -]
        public BasePerson()
        {

        }
        public BasePerson(int id, string fName, string lName)
        {
            Id = id;
            FName = fName;
            LName = lName;
        }
        #endregion

        public int Id { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
    }
}
