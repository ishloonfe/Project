﻿using Project.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Base
{
    public class BaseProduct : IProduct <int,string>
    {
        #region [- ctor -]
        public BaseProduct()
        {

        }
        public BaseProduct( int id, string title, int unitPrice)
        {
            Id = id;
            Title = title;
            UnitPrice = unitPrice;
        }
        #endregion

        public int Id { get; set; }
        public string Title { get; set; }
        public int UnitPrice { get; set; }
    }
}
