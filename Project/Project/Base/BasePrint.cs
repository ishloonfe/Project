﻿using Project.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Base
{
    public class BasePrint : IPrint
    {
        #region [- ctor -]
        //public BasePrint(int parameter)
        //{
        //    Print(parameter);
        //}

        public BasePrint(int parameter, Action<int, string, string> initialSeller, Action<int, string, string> initialBuyer) 
        {
            Print(parameter, initialBuyer, initialSeller);
        }
        #endregion

        #region [- Print -]
        public void Print(int parameter, Action<int, string, string> initialBuyer, Action<int, string, string> initialSeller)
        {
            Console.WriteLine(parameter);
            Console.WriteLine(initialBuyer);
            Console.WriteLine(initialSeller);
        } 
        #endregion

    }
}
