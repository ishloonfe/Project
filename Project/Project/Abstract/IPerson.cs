﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Abstract
{
    public interface IPerson <U, T>
    {
        U Id { get; set; }
        T FName { get; set; }
        T LName { get; set; }
    }
}
