﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Abstract
{
    public interface IProduct <U,T>
    {
        U Id { get; set; }
        T Title { get; set; }
        U UnitPrice { get; set; }
    }
}
