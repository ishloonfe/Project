﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Abstract
{
    public interface IOrder <U,T,B>
    {
        U Id { get; set; }
        U TotalPrice { get; }
        T BaseSeller { get; set; }
        T BaseBuyer { get; set; }
        B BaseOrderDetails { get; set;}
    }
}
