﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Abstract
{
    public interface IOrderDetail <U>
    {
        U Quantity { get; set; }
        U Price { get; }
    }
}
