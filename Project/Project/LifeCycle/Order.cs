﻿using Project.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.LifeCycle
{
    public class Order : BaseOrder
    {
        #region [- ctor -]
        public Order(List<BaseOrderDetail> baseOrderDetails) : base(baseOrderDetails)
        {

        } 
        #endregion
    }
}
