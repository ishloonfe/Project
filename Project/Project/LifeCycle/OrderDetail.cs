﻿using Project.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.LifeCycle
{
    public class OrderDetail : BaseOrderDetail
    {
        #region [- ctor -]
        public OrderDetail(BaseProduct baseProduct, int quantity): base(baseProduct,quantity)
        {

        } 
        #endregion
    }
}
