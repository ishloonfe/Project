﻿using Project.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.LifeCycle
{
    public class Print : BasePrint
    {
        #region [- ctor -]
        public Print(int parameter , Action<int, string, string> initialSeller, Action<int, string, string> initialBuyer) : base(parameter,initialBuyer,initialSeller)
        {

        } 
        #endregion
    }
}
